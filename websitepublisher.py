#!/usr/bin/env python3
# SPDX-License-Identifier: BSD-2-Clause
#
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileCopyrightText: 2023 Ben Cooksley <bcooksley@kde.org>
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import sys

from sftpnotary import authentication, config, util
from sftpnotary.exceptions import Error
from sftpnotary.websitepublisher import PublishWebsiteWorker


def parseCommandLine():
    import argparse

    parser = argparse.ArgumentParser(description="Worker for publishing files to a remote server")

    # debug options
    parser.add_argument("-v", "--verbose", action="count", default=0, help="increase the verbosity")

    parser.add_argument("--id", dest="workerId", help="Unique, alpha-numeric identifier of this service worker")

    parser.add_argument("--config")

    options = parser.parse_args()
    return options


def setUpAuthentication():
    auth = authentication.GitLabJobTokenAuthentication(
        apiV4Url=config.settings.get("Authentication", "GitLabAPIv4Url", ""), userAgent="org.kde.websitepublisher"
    )
    authentication.setAuthentication(auth)


def startWorker(workerId: str):
    with util.connectToSftpServer() as sftp:
        worker = PublishWebsiteWorker(workerId, sftp)
        worker.start()


def main():
    options = parseCommandLine()
    config.loadConfig(options.config)
    util.setUpServiceLogging("websitepublisher", options.verbose)
    util.loadProjectSettings(config.settings.get("General", "ProjectSettings"))
    if config.settings.getboolean("Authentication", "Enabled", False):
        setUpAuthentication()

    startWorker(options.workerId)

    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Error as e:
        print("Error: %s" % e, file=sys.stderr)
        sys.exit(1)
