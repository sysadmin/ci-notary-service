# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2023 KDE e.V.

-r requirements.txt

azure-storage-blob
markdownify

git+https://invent.kde.org/kloecker/microstore.git
