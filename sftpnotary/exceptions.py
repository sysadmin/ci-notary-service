# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>


class Error(Exception):
    pass


class InvalidFileName(Error):
    def __init__(self, fileName=None):
        super().__init__(fileName)
        self.fileName = fileName

    def __str__(self):
        if self.fileName is None:
            return "No file name"
        else:
            return f"Invalid file name: {self.fileName!r}"


class InvalidParameter(Error):
    def __init__(self, name, value=None):
        super().__init__(name, value)
        self.name = name
        self.value = value

    def __str__(self):
        if self.value is None:
            return f"Parameter not specified: {self.name!r}"
        else:
            return f"Invalid parameter '{self.name!r}' value: {self.value!r}"
