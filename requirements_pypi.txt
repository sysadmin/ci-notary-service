# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2023 KDE e.V.

-r requirements.txt

pkginfo
twine
