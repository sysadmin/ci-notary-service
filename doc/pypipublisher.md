<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileCopyrightText: 2023 Ben Cooksley <bcooksley@kde.org>
# SPDX-FileCopyrightText: 2024 Julius Künzel <julius.kuenzel@kde.org>
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# pypipublisher.py - A service to publish Python distribution packages

This service takes an archive of distribution files (like *.whl), unpacks it and publishes them
with [twine](https://twine.readthedocs.io/en/stable/index.html#) to a Python repository.

## Prerequisites

The service needs an SFTP server for exchanging data with the clients.

For publishing to the remote repository, a `.pypirc` config needs to be in place.
Inside this configuration file the credentials for the remote repositories need to be set.
The service currently supports two repositories: https://test.pypi.org and https://pypi.org

Hence your config file may look like this:

```
[pypi]
username = __token__
password = <PyPI token> # Replace by the real api token

[testpypi]
username = __token__
password = <TestPyPI token> # Replace by the real api token
```

The name of the file does not need to be `.pypirc`, but its path needs to be set in `pypipublisher.ini`.

For more details on the config file, read the [documentation](https://packaging.python.org/en/latest/specifications/pypirc/).

## Installation

### Install the Service

Clone this repository on a server that shall provide the service,
create a virtual environment, and install the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git pypipublisher
cd pypipublisher
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements_pypi.txt
```

### Configure the Service

Copy `pypipublisher.sample.ini` to `pypipublisher.ini` and replace the example values
with the appropriate values for your installation.

Then copy `pypipublisher-projects.sample.yaml` to `pypipublisher-projects.yaml` and
specify the settings for the projects you want to use the service for.

## Run the Service

Activate the virtual environment and start `pypipublisher.py`.

```sh
cd ~/pypipublisher
. .venv/bin/activate
python3 pypipublisher.py --id workerA --config pypipublisher.ini
```

where you replace `workerA` with a unique alpha-numeric ID for each instance
of `pypipublisher.py` you start.

## Use the Service to Publish a Package

Clone this repository on the client, create a virtual environment, and install
the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git pypipublisher
cd pypipublisher
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

Copy `publishonpypi.sample.ini` to `publishonpypi.ini` and replace the example values
with the appropriate values for your installation. In particular, the value
for `BasePath` must match the corresponding value in the settings of the
service.

Then run `publishonpypi.py` to publish the package.

```sh
python3 publishonpypi.py --config publishonpypi.ini --repository <repository name> <path to files to be published remotely>
```
